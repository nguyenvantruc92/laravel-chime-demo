<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'chime' => [
        'region' =>  env('AWS_CHIME_REGION', 'us-east-1'),
        'sns_topic_arn' =>  env('AWS_CHIME_SNS_TOPIC_ARN'),
        'default_credential' => env('AWS_CHIME_DEFAULT_CREDENTIAL', 1),
        'key' =>  env('AWS_CHIME_KEY'),
        'secret' =>  env('AWS_CHIME_SECRET'),
        'ecs_cluster_arn' => env('ECS_CLUSTER_ARN'),
        'recording_artifacts_bucket' => env('RECORDING_ARTIFACTS_BUCKET'),
        'ecs_container_name' => env('ECS_CONTAINER_NAME'),
        'ecs_task_definition_arn' => env('ECS_TASK_DEFINITION_ARN'),
        'ecs_region' => env('ECS_REGION'),
        'ecs_security_group' => env('ECS_SECURITY_GROUP'),
        'ecs_subnet' => env('ECS_SUBNET'),
    ],
];
