<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class WebhookController extends BaseController
{
    const JOINED = 'joined';
    const LEFT = 'left';
    const ENDED = 'ended';
    const ATTENDEE_JOIN = 'chime:AttendeeJoined';
    const ATTENDEE_LEAVE = 'chime:AttendeeLeft';
    const MEETING_ENDED = 'chime:MeetingEnded';

    public function event(Request $request)
    {
        if (!$this->validate()) {
            return false;
        }
        $data = json_decode($request->getContent(), true);
        $message = json_decode($data["Message"], true);
        $detail = $message['detail'];
        $type = $detail['eventType'];
        switch ($type) {
            case self::ATTENDEE_JOIN:
                $this->join($detail);
                break;
            case self::ATTENDEE_LEAVE:
                $this->left($detail);
                break;
            case self::MEETING_ENDED:
                //
                break;
        }
        return true;
    }

    private function validate() : bool
    {
        try {
            $message = Message::fromRawPostData();
            $validator = new MessageValidator();
            $validator->validate($message);
        } catch (\Exception $ex) {
            return false;
        }
        return true;
    }

    private function join($detail)
    {
        $participant = Participant::where('external_user_id', $detail['externalUserId'])->first();
        if ($participant) {
            $participant->status = self::JOINED;
            $participant->joined_at = now();
            $participant->save();
        }
    }

    private function left($detail)
    {
        $participant = Participant::where('external_user_id', $detail['externalUserId'])->first();
        if ($participant) {
            $participant->status = self::LEFT;
            $participant->leaved_at = now();
            $participant->duration = strtotime($participant->leaved_at) - strtotime($participant->joined_at);
            $participant->save();
        }
    }
}
