<?php

namespace App\Http\Controllers;

use App\Models\Meeting;
use App\Models\Participant;
use App\Services\IMeetingService;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class MeetingController extends BaseController
{
    protected IMeetingService $meetingService;

    public function __construct(IMeetingService $meetingService)
    {
        $this->meetingService = $meetingService;
    }

    public function join(Request $request)
    {
        DB::beginTransaction();
        try {
            $title = $request->title;
            $name = $request->name;
            $data = [
                'ClientRequestToken' => $title,
                'ExternalMeetingId' => $title
            ];
            $meeting = $this->meetingService->createMeeting($data);
            $meetingId = $meeting['MeetingId'];
            $externalUserId = substr(Uuid::uuid4(), 0, 8) . '#' . substr($name, 0, 64);
            $attendee = $this->meetingService->createAttendee($meeting['MeetingId'], $externalUserId);
            $meetingData = $this->createMeeting($title, $meetingId);
            $this->createParticipant($meetingData->id, $name, $externalUserId);
            DB::commit();
            return [
                'JoinInfo' => [
                    'Meeting' => [
                        'Meeting' => $meeting,
                    ],
                    'Attendee' => [
                        'Attendee' => $attendee
                    ]
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }

    }

    private function createMeeting(string $name, string $meetingId)
    {
        return Meeting::create([
            'name' => $name,
            'meeting_id' => $meetingId,
            'status' => 0
        ]);
    }

    private function createParticipant(string $meetingId, string $name, string $externalUserId)
    {
        return Participant::create([
            'meeting_id' => $meetingId,
            'name' => $name,
            'external_user_id' => $externalUserId,
        ]);
    }
}
