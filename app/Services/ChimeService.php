<?php

namespace App\Services;

use Aws\Chime\ChimeClient;
use Aws\Credentials\Credentials;
use Aws\Credentials\CredentialProvider;
use Aws\Chime\Exception\ChimeException;

class ChimeService implements IMeetingService
{
    const VERSION = 'latest';

    const MAX_RESULT_LIST_ATTENDEE = 50;

    private ChimeClient $chime;

    private $region;

    private $snsTopicArn;

    public function __construct()
    {
        $this->region = config('services.chime.region');
        $this->snsTopicArn = config('services.chime.sns_topic_arn');
        $isDefaultCredential = config('services.chime.default_credential');
        if ((bool) $isDefaultCredential) {
            $credentials = CredentialProvider::defaultProvider();
        } else {
            $credentials = new Credentials(config('services.chime.key'), config('services.chime.secret'));
        }
        $this->chime = new ChimeClient([
            'region' => $this->region, //us-east-1
            'version' => self::VERSION, //latest
            'credentials' => $credentials //new Credentials('AWS_CHIME_KEY', 'AWS_CHIME_SECRET')
        ]);
    }

    public function createMeeting(array $data)
    {
        dd($this->addDataDefaultCreateMeeting($data));
        $meeting = $this->chime->createMeeting($this->addDataDefaultCreateMeeting($data));
        return $meeting->get('Meeting');
    }

    public function getMeeting(string $meetingId) : ?array
    {
        try {
            $meeting = $this->chime->getMeeting(['MeetingId' => $meetingId]);
            return $meeting->get('Meeting');
        } catch (ChimeException $ex) {
            return ['error_code' => $ex->getStatusCode(), 'message' => $ex->getAwsErrorMessage()];
        }

    }

    public function createAttendee(string $meetingId, string $externalUserId)
    {
        $attendee = $this->chime->createAttendee([
            'MeetingId' => $meetingId,
            'ExternalUserId' => $externalUserId,
        ]);
        return $attendee->get('Attendee');
    }

    private function addDataDefaultCreateMeeting($data)
    {
        $data['MediaRegion'] = $this->region;
        $data['NotificationsConfiguration'] = [
            'SnsTopicArn' => $this->snsTopicArn
        ];
        return $data;
    }

    public function listAttendees(string $meetingId, int $maxResults = 50, string $nextToken = null) : array
    {
        $data = [
            'MeetingId' => $meetingId,
            'MaxResults' => $maxResults ?? self::MAX_RESULT_LIST_ATTENDEE
        ];
        if (!empty($nextToken)) {
            $data['NextToken'] = $nextToken;
        }
        $attendees = $this->chime->listAttendees($data);
        return ['Attendees' => $attendees->get('Attendees'), 'NextToken' => $attendees->get('NextToken')];
    }
}
