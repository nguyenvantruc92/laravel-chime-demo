<?php

namespace App\Services;

interface IMeetingService
{
    function createMeeting(array $data);
    
    function getMeeting(string $meetingId) : ?array; 

    function createAttendee(string $meetingId, string $externalUserId);

    function listAttendees(string $meetingId, int $maxResults = 50, string $nextToken = null) : array;
}