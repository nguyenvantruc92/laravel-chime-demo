<?php

namespace App\Http\Controllers;

use App\Models\Meeting;
use App\Models\Participant;
use Aws\Chime\ChimeClient;
use Aws\Credentials\Credentials;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class MeetingDemoController extends BaseController
{
    protected $chime;

    public function __construct()
    {
        $this->chime = new ChimeClient([
            'region' => 'us-east-1',
			'version' => 'latest',
			'credentials' => new Credentials('xxx', 'xxxx')
		]);
    }

    public function join(Request $request)
    {
        DB::beginTransaction();
        try {
            $title = $request->title;
            $name = $request->name;

            //Tạo mới 1 meeting sử dụng Amazon Chime API
            $meetingData = [
                //Token này là duy nhất. Nếu token này khác nhau sẽ tạo ra 2 meeting khác nhau
                "ClientRequestToken" => "meeting1",
                //Hiện tại Amazon Chime SDK chỉ hỗ trợ SNS và SQS cho US East (N. Virginia) (us-east-1) AWS Region
                "MediaRegion" => "us-east-1",
                "NotificationsConfiguration" => [
                    //SnsTopicArn => AWS SNS sẽ gọi webhook để thông báo khi các sự kiện meeting vào attendee xảy ra.
                    "SnsTopicArn" => "arn:aws:sns:us-east-1:xxx:xxx"
                ]
            ];
            $meetingResult = $this->chime->createMeeting($meetingData);
            $meeting = $meetingResult->get('Meeting');
            $meetingId = $meeting['MeetingId'];

            //Tạo mới 1 attendee sử dụng Amazon Chime API
            //Hiện tại Chime SDK không cho phép định nghĩa thêm thông tin cho Attendee. Những thông ti như email,
            //avatar, phone, ... phải được lưu ở phía ứng dụng và mapping với nhau thông qua ExternalUserId
            $externalUserId = substr(Uuid::uuid4(), 0, 8) . '#' . substr($name, 0, 64);
            $attendeeData = [
                'MeetingId' => $meetingId,
                'ExternalUserId' => $externalUserId,
            ];
            $attendeeResult = $this->chime->createAttendee($attendeeData);
            $attendee = $attendeeResult->get('Attendee');

            //Lưu thông tin vào database
            $meetingData = Meeting::where('meeting_id', $meetingId)->first();
            if (empty($meetingData)) {
                $meetingData = Meeting::create(['name' => $title, 'meeting_id' => $meetingId, 'status' => 0]);
            }
            Participant::create(['meeting_id' => $meetingData->id, 'name' => $name, 'external_user_id' => $externalUserId]);
            DB::commit();
            return [
                'JoinInfo' => [
                    'Meeting' => [
                        'Meeting' => $meeting,
                    ],
                    'Attendee' => [
                        'Attendee' => $attendee
                    ]
                ]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }
}
