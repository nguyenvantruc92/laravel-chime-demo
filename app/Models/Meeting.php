<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{

    protected $fillable = [
        'name',
        'meeting_id',
        'status'
    ];
}
