<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('external_user_id')->nullable();
            $table->integer('duration')->default(0);
            $table->enum('status', ['added', 'joined', 'left', 'ended'])->index('status')->default('added');
            $table->dateTime('joined_at')->nullable();
            $table->dateTime('leaved_at')->nullable();
            $table->bigInteger('meeting_id')->unsigned()->nullable();
            $table->foreign('meeting_id', 'meeting_participants_ibfk_1')
                ->references('id')
                ->on('meetings')
                ->onUpdate('RESTRICT')
                ->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
